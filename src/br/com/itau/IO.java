package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO {

    public static Map<String, String> solicitarDados(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite seu nome: ");
        String nome = scanner.nextLine();

        System.out.println("Digite seu cpf: ");
        String cpf = scanner.nextLine();

        System.out.println("Digite sua idade: ");
        String idade = scanner.nextLine();

        Map<String, String> dados = new HashMap<>();
        dados.put("nome", nome);
        dados.put("cpf", cpf);
        dados.put("idade", idade);

        return dados;
    }

    public static void exibeCliente(Cliente cliente){
        System.out.println("Nome: " + cliente.getNome());
        System.out.println("CPF: " + cliente.getCpf());
        System.out.println("Idade: " + cliente.getIdade());

        System.out.println("********************");
    }

    public static Integer solicitarOperacao(){
        Integer opcao;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Escolha a operação que deseja realizar:");
        System.out.println("1 - Depositar");
        System.out.println("2 - Saque");
        System.out.println("3 - Ver saldo");
        System.out.println("4 - Sair");

        opcao = Integer.parseInt(scanner.nextLine());
        return(opcao);
    }

    public static Double lerValor(){
        Double valor;
        Scanner scanner = new Scanner(System.in);

        valor = scanner.nextDouble();
        return(valor);
    }
}
