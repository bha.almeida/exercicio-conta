package br.com.itau;

public class Conta {
    private int numero;
    private Cliente cliente;
    private double saldo = 0;

    public Conta(int numero, Cliente cliente) {
        this.numero = numero;
        this.cliente = cliente;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double sacar(Double valor){
        this.saldo = saldo - valor;
        return saldo;
    }

    public double depositar(Double valor){
        this.saldo = saldo + valor;
        return saldo;
    }

    public void verSaldo(){
        System.out.println("saldo da conta: " +this.saldo);
    }
}
