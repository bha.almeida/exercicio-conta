package br.com.itau;

import java.util.Map;

public class Main {

    public static void main(String[] args) {

        Map<String, String> dados = IO.solicitarDados();

        Cliente cliente = new Cliente(
                dados.get("nome"),
                dados.get("cpf"),
                Integer.parseInt(dados.get("idade"))
        );

        IO.exibeCliente(cliente);

        if (cliente.validaIdade(cliente.getIdade())) {
            Conta conta = new Conta(
                    12345,
                    cliente
            );

            int opcao = IO.solicitarOperacao();

            while (opcao != 4){
                if (opcao == 1) {
                    System.out.println("Digite o valor do depósito:");
                    conta.depositar(IO.lerValor());
                    opcao = IO.solicitarOperacao();
                } else if (opcao == 2) {
                    System.out.println("Digite o valor do saque:");
                    conta.sacar(IO.lerValor());
                    opcao = IO.solicitarOperacao();
                } else if (opcao == 3) {
                    conta.verSaldo();
                    opcao = IO.solicitarOperacao();
                }
            }
        } else {
            System.out.println("Você não possui idade suficiente para abertura da conta");
        }

    }
}
